create DATABASE if not exists blogdb;
use blogdb;

create TABLE if not exists BlogPost (
 PostID SMALLINT,
 titleOfpost VARCHAR(50),
 MesseageOfPost TEXT,
 TimeStamp TIMESTAMP,
 WhoPosted BLOB,
 LastEdit DATETIME,
 PostViewable BIT(2),
 PRIMARY KEY (PostID)
 );

create TABLE if not exists BlogComment (
 CommentID SMALLINT,
 WhoPosted BLOB,
 MessageOfComment TEXT,
 TimeStamp TIMESTAMP,
 PRIMARY KEY (CommentID)
 );

create TABLE if not exists Users (
 Username VARCHAR (30),
 UserEmail VARCHAR (50),
 UserRealName VARCHAR (100),
 UserGender BIT(2),
 UserAge SMALLINT,
 Password VARCHAR (30),
 Salt VARCHAR (10),
 PRIMARY KEY (Username)
 );

create TABLE if not exists Categories (
 CategoryID SMALLINT,
 CategoryName BLOB,
 PRIMARY KEY (CategoryID)
 );

create TABLE if not exists BlogpostCategory (
 PostID SMALLINT,
 CategoryID SMALLINT,
 PRIMARY KEY (PostID, CategoryID)
 );
