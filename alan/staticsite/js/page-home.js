(function($){})(window.jQuery);
$(document).ready(function (){
    $('.slider').not('.slick-initialized').slick({
	autoplay: true,
	dots: true,
	lazyLoad: 'ondemand',
	autoplayspeed: 2000,
	infinite: true,
	fade: true,
	slidesToShow: 1,
	slidesToScroll: 1,
    });
});
